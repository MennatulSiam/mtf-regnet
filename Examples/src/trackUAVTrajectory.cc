#include "mtf/mtf.h"

// tools for reading in images from various sources like image sequences, 
// videos and cameras as well as for pre processing them
#include "mtf/Tools/pipeline.h"
// general OpenCV tools for selecting objects, reading ground truth, etc.
#include "mtf/Tools/cvUtils.h"

#include "mtf/Config/parameters.h"
#include "mtf/Config/datasets.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime>
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

//++++++
#include <fstream>
#include <Eigen/Dense>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#ifndef MTF_AM
#define MTF_AM SSD
#endif

#ifndef MTF_SSM
#define MTF_SSM Similitude
#endif

#define MAX_FPS 1e6

using namespace Eigen;

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {

	if(!readParams(argc, argv)){ return EXIT_FAILURE; }

#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif

	actor = "Synthetic";
	source_name = "uav_sim";
	source_path = db_root_path + "/" + actor;

	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name.c_str());
	printf("actor: %s\n", actor.c_str());
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("show_cv_window: %d\n", show_cv_window);
	printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
	printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
	//printf("read_obj_from_gt: %d\n", read_obj_from_gt);
	//printf("write_tracking_data: %d\n", write_tracking_data);
	printf("mtf_sm: %s\n", mtf_sm);
	printf("mtf_am: %s\n", mtf_am);
	printf("mtf_ssm: %s\n", mtf_ssm);
	printf("********************************\n");

	/* initialize pipeline*/
	InputBase *input = getInputObj();
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully\n");
		return EXIT_FAILURE;
	}
	string satellite_img_file = string(source_path) + "/" + string(source_name) + "/YJ_map_right.bmp";
	printf("satellite_img_file: %s\n", satellite_img_file.c_str()); 
	cv::Mat satellite_img = cv::imread(satellite_img_file);

	cv::Point fps_origin(10, 20);
	double fps_font_size = 1.00;
	cv::Scalar fps_color(0, 255, 0);
	cv::Point err_origin(10, 40);
	double err_font_size = 1.00;
	cv::Scalar err_color(0, 255, 0);
	cv::Scalar gt_color(0, 255, 0);

	cv::Mat entire_image(2, 4, CV_64FC1);
	// leave one pixel border arund the bounding box 
	// so numerical image derivatives can be computed more easily
	entire_image.at<double>(0, 0) = 1;
	entire_image.at<double>(0, 1) = input->getFrame().cols - 2;
	entire_image.at<double>(0, 2) = input->getFrame().cols - 2;
	entire_image.at<double>(0, 3) = 1;
	entire_image.at<double>(1, 0) = 1;
	entire_image.at<double>(1, 1) = 1;
	entire_image.at<double>(1, 2) = input->getFrame().rows - 2;
	entire_image.at<double>(1, 3) = input->getFrame().rows - 2;

	/*********************************** initialize tracker ***********************************/
	string gt_file = string(source_path) + "/" + string(source_name) + "/initial_location.txt";
	ifstream fin(gt_file);
	float ulx, uly, urx, ury, lrx, lry, llx, lly;
	fin >> ulx >> uly >> urx >> ury >> lrx >> lry >> llx >> lly;
	cout << ulx << "\t" << uly << "\t" << urx << "\t" << ury << "\t" << lrx << "\t" << lry << "\t" << llx << "\t" << lly << "\n";

	cv::Mat init_location(2, 4, CV_64FC1);
	init_location.at<double>(0, 0) = ulx;
	init_location.at<double>(0, 1) = urx;
	init_location.at<double>(0, 2) = lrx;
	init_location.at<double>(0, 3) = llx;
	init_location.at<double>(1, 0) = uly;
	init_location.at<double>(1, 1) = ury;
	init_location.at<double>(1, 2) = lry;
	init_location.at<double>(1, 3) = lly;

    //+++++++++++++++++++initial the center point of init_location+++++++++++++++++++
    //cv::Mat init_ctr(1,2,CV_64FC1);
    //init_ctr.at<double>(0,0) = 1444;
    //init_ctr.at<double>(0,1) = 612;
    cv::Point2d init_corner;
    init_corner.x = 1444;
    init_corner.y = 612;

    printf("////////////////////////////////////\n");
    Matrix2Xd init_pt(2,1);
    init_pt<< 1444,612;

	hom_normalized_init = 0;
    //ctr_pts.push_back(init_corner);
    //+++++++++++++++++++read ground truth key points++++++++++++++++++++++++++++++++

	printf("+++++++++++++++read ground truth trajectory key points+++++++++++++\n");

	string gt_kpt_flnm = string(source_path) + "/" + string(source_name) + "/uav_trajectory_groundtruth.txt";
	ifstream gt_kpt(gt_kpt_flnm);
	vector<cv::Point2d> gt_kpt_corners;
	cv::Point2d tmp;
	int i = 0;
    while(gt_kpt >> tmp.x >> tmp.y)
    {
        gt_kpt_corners.push_back(tmp);
        cout<<gt_kpt_corners.at(i).x<<" "<<gt_kpt_corners.at(i).y<<endl;
        i = i+1;
    }
    //printf("The number of ground truth trajectory key points: %s", gt_kpt_corners.size());
    printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if defined USE_OLD_METHOD
	mtf::ImgParams img_params(resx, resy, grad_eps, hess_eps);
	mtf::FCLKParams fclk_params(max_iters, epsilon,
		static_cast<mtf::FCLKParams::HessType>(hess_type), sec_ord_hess,
		update_templ, fc_chained_warp, debug_mode);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++MI
	mtf::MIParams mi_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou,
		getPixMapperObj(pix_mapper, &img_params), debug_mode);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++MI
	mtf::FCLK<mtf::MI, mtf::Similitude> uav_tracker(&fclk_params, &mi_params);
#elif defined USE_TEMPLATED_SM
	typedef mtf::SearchMethod<mtf::MTF_AM, mtf::MTF_SSM> TrackerType;
	TrackerType *tracker = dynamic_cast<TrackerType*>(mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm));
#else
	mtf::nt::SearchMethod *tracker = mtf::getSM(mtf_sm, mtf_am, mtf_ssm);
#endif
	if(!tracker){
		printf("Tracker could not be initialized successfully\n");
		return EXIT_FAILURE;
	}	
#ifdef USE_TEMPLATED_SM
	TrackerType &uav_tracker = *tracker;
#else
	mtf::nt::SearchMethod &uav_tracker = *tracker;
#endif

	/* initialize frame pre processors */
	GaussianSmoothing uav_pre_proc(uav_tracker.inputType()), satellite_pre_proc(uav_tracker.inputType());	
	satellite_pre_proc.initialize(satellite_img);
	uav_tracker.initialize(satellite_pre_proc.getFrame(), init_location);

	uav_pre_proc.initialize(input->getFrame());

#if defined USE_OLD_METHOD
	//=======Mutual information
	mtf::ImgParams uav_img_params(resx, resy, grad_eps, hess_eps);
	mtf::MIParams uav_mi_params(&uav_img_params, mi_n_bins, mi_pre_seed, mi_pou,
		nullptr, debug_mode);
	mtf::MI template_extractor_am(&uav_mi_params);
	//mtf::SSD template_extractor_am(&uav_img_params);
	mtf::Translation template_extractor_ssm(resx, resy);
	template_extractor_ssm.initialize(entire_image);
	template_extractor_am.setCurrImg(uav_pre_proc.getFrame());
	template_extractor_am.initializePixVals(template_extractor_ssm.getPts());
#endif

	string cv_win_name = "OpenCV Window :: " + source_name;
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
	}

	double fps = 0, fps_win = 0;
	double tracking_time, tracking_time_with_input;
	double avg_fps = 0, avg_fps_win = 0;
	int fps_count = 0;
	double avg_err = 0;

	double tracking_err = 0;
	int valid_frame_count = 0;
	cv::Point2d corners[4];

	CVUtils cv_utils;
	cv::Mat satellite_img_copy, satellite_img_small;
	satellite_img_small.create(1000, 900, satellite_img.type());

    // ++++++++draw ground truth trajectory on the image++++++++++++
    cv::Mat satellite_img_copy1;
    satellite_img.copyTo(satellite_img_copy1);
    for(i = 0; i < gt_kpt_corners.size()-2; i++)
    {
        line(satellite_img_copy1, gt_kpt_corners.at(i), gt_kpt_corners.at(i+1), CV_RGB(0,255,0), 2);
    }
    line(satellite_img_copy1, gt_kpt_corners.at(gt_kpt_corners.size()-2), gt_kpt_corners.at(gt_kpt_corners.size()-1), CV_RGB(0,255,0), 2);

	/*********************************** update tracker ***********************************/
    vector<cv::Point2d> ctr_pts;
	while(true) {

		satellite_img_copy1.copyTo(satellite_img_copy);

		if(record_frames || show_cv_window){
			/* draw tracker positions to OpenCV window */
			printf("----------------------------------------\n");
			for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
                //printf("test: %d\n",n_trackers);
				int col_id = tracker_id % cv_utils.no_of_cols;
				cv_utils.cornersToPoint2D(corners, uav_tracker.getRegion());
				line(satellite_img_copy, corners[0], corners[1], cv_utils.obj_cols[col_id], line_thickness);
				line(satellite_img_copy, corners[1], corners[2], cv_utils.obj_cols[col_id], line_thickness);
				line(satellite_img_copy, corners[2], corners[3], cv_utils.obj_cols[col_id], line_thickness);
				line(satellite_img_copy, corners[3], corners[0], cv_utils.obj_cols[col_id], line_thickness);
				putText(satellite_img_copy, uav_tracker.name, corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, cv_utils.obj_cols[col_id]);

                Matrix2Xd warped_pts(2,1);
				VectorXd curr_ssm_state(uav_tracker.getSSM()->getStateSize());
				uav_tracker.getSSM()->estimateWarpFromCorners(curr_ssm_state, init_location, uav_tracker.getRegion());
				uav_tracker.getSSM()->applyWarpToPts(warped_pts, init_pt, curr_ssm_state);
                cv::Point2d tmpPt;
                tmpPt.x = warped_pts(0,0);
                tmpPt.y = warped_pts(1,0);

                printf("x: %f, y: %f\n",tmpPt.x,tmpPt.y);
                //line(satellite_img_copy1, gt_kpt_corners.at(i), gt_kpt_corners.at(i+1), CV_RGB(0,255,0), 3);
                if(ctr_pts.size()>1)
                {
                    line(satellite_img_copy1,tmpPt, ctr_pts.at(ctr_pts.size()-1),CV_RGB(255,0,0), 2);
                }
                ctr_pts.push_back(tmpPt);
                //printf("The size of ctr_pts: %d\n", ctr_pts.size());
			}
			//if(show_ground_truth){
			//	cv_utils.cornersToPoint2D(corners, cv_utils.ground_truth[input->getFrameID()]);
			//	line(input->getFrameMutable(), corners[0], corners[1], gt_color, line_thickness);
			//	line(input->getFrameMutable(), corners[1], corners[2], gt_color, line_thickness);
			//	line(input->getFrameMutable(), corners[2], corners[3], gt_color, line_thickness);
			//	line(input->getFrameMutable(), corners[3], corners[0], gt_color, line_thickness);
			//	putText(input->getFrameMutable(), "ground_truth", corners[0],
			//		cv::FONT_HERSHEY_SIMPLEX, fps_font_size, gt_color);
			//}
			char fps_text[100];
			snprintf(fps_text, 100, "frame: %d c: %9.3f a: %9.3f cw: %9.3f aw: %9.3f fps",
				input->getFrameID() + 1, fps, avg_fps, fps_win, avg_fps_win);
			putText(satellite_img_copy, fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

			if(show_tracking_error){
				char err_text[100];
				snprintf(err_text, 100, "ce: %12.8f ae: %12.8f", tracking_err, avg_err);
				putText(satellite_img_copy, err_text, err_origin, cv::FONT_HERSHEY_SIMPLEX, err_font_size, err_color);
			}

			if(show_cv_window){
				cv::resize(satellite_img_copy, satellite_img_small, cv::Size(satellite_img_small.cols, satellite_img_small.rows));
				imshow(cv_win_name, satellite_img_small);
				imshow("UAV", input->getFrame());
				int pressed_key = cv::waitKey(1 - pause_after_frame);
				if(pressed_key == 27){
					break;
				}
				if(pressed_key == 32){
					pause_after_frame = 1 - pause_after_frame;
				}
			}
		}
		if(!show_cv_window && (input->getFrameID() + 1) % 50 == 0){
			printf("frame_id: %5d avg_fps: %15.9f avg_fps_win: %15.9f avg_err: %15.9f\n",
				input->getFrameID() + 1, avg_fps, avg_fps_win, avg_err);
		}
		if( input->n_frames > 0 && input->getFrameID() >=  input->n_frames - 1){
			printf("==========End of input stream reached==========\n");
			break;
		}

		mtf_clock_get(start_time_with_input);
		// update frame
		if (!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFrameID() + 1);
			break;
		}
		uav_pre_proc.update(input->getFrame(), input->getFrameID());

		// extract template
#ifdef USE_OLD_METHOD
		template_extractor_am.updatePixVals(template_extractor_ssm.getPts());
		uav_tracker.getAM()->setInitPixVals(template_extractor_am.getCurrPixVals());
#else
		cv::Mat curr_uav_pos = uav_tracker.getRegion().clone();
		uav_tracker.initialize(uav_pre_proc.getFrame(), entire_image);
		uav_tracker.setRegion(curr_uav_pos);
#endif
		cv::Mat curr_img_cv = cv::Mat(uav_tracker.getAM()->getResY(), uav_tracker.getAM()->getResX(), CV_64FC1,
			const_cast<double*>(uav_tracker.getAM()->getInitPixVals().data()));
		cv::Mat  curr_img_cv_uchar(uav_tracker.getAM()->getResY(), uav_tracker.getAM()->getResX(), CV_8UC1);
		curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
		imshow("Template", curr_img_cv_uchar);

		//update tracker
		mtf_clock_get(start_time);
		uav_tracker.update(satellite_pre_proc.getFrame());
		mtf_clock_get(end_time);
		mtf_clock_measure(start_time, end_time, tracking_time);
		mtf_clock_measure(start_time_with_input, end_time,
			tracking_time_with_input);
		fps = 1.0 / tracking_time;
		fps_win = 1.0 / tracking_time_with_input;

		if(!std::isinf(fps) && fps<MAX_FPS){
			++fps_count;
			avg_fps += (fps - avg_fps) / fps_count;
			// if fps is not inf then fps_win too must be non inf
			avg_fps_win += (fps_win - avg_fps_win) / fps_count;
		}
	}//end while(true)

	printf("Average FPS: %15.10f\n", avg_fps);
	printf("Average FPS with Input: %15.10f\n", avg_fps_win);
	if(show_tracking_error){
		printf("Average Tracking Error: %15.10f\n", avg_err);
		printf("Frames used for computing the average: %d\n", valid_frame_count);
	}
	string outfl = string(source_path) + "/" + string(source_name) + "/uav_trajectory_traj_results.txt";
	ofstream outtrajectory(outfl);
	//if(!outtrajectory)return;
	for(i = 0;i<ctr_pts.size();i++)
	{
        outtrajectory<<ctr_pts.at(i).x<<" "<<ctr_pts.at(i).y<<endl;
	}
    outtrajectory.close();
    printf("The number of points: %d\n",ctr_pts.size());

	cv::waitKey(0);
	cv::destroyAllWindows();
	return 0;
}
