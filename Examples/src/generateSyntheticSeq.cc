/**
application to generate synthetic sequences by warping an image using
random perturbations produced by different SSMs
*/

#include "mtf/mtf.h"

// tools for reading in images from various sources like image sequences, 
// videos and cameras as well as for pre processing them
#include "mtf/Tools/pipeline.h"
// general OpenCV tools for selecting objects, reading ground truth, etc.
#include "mtf/Tools/cvUtils.h"
#include "mtf/Config/parameters.h"
#include "mtf/Utilities/miscUtils.h"
#include "mtf/Utilities/warpUtils.h"
#include "mtf/Utilities/imgUtils.h"

#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

using namespace Eigen;

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {

	printf("Starting MTF synthetic sequence generator...\n");

	if(!readParams(argc, argv)){ return EXIT_FAILURE; }

#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif

	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name.c_str());
	printf("actor: %s\n", actor.c_str());
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("syn_show_output: %d\n", syn_show_output);
	printf("mtf_ssm: %s\n", mtf_ssm);
	printf("********************************\n");

	/* initialize pipeline*/
	InputBase *input = getInputObj(pipeline);
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully\n");
		return EXIT_FAILURE;
	}
	bool init_obj_read = false;
	CVUtils cv_utils;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		obj_struct* init_object = cv_utils.readObjectFromGT(source_name, source_path, input->n_frames,
			init_frame_id, use_opt_gt, opt_gt_ssm, use_reinit_gt, debug_mode);
		if(init_object){
			init_obj_read = true;
			init_objects.push_back(init_object);
		} else{
			printf("Failed to read initial object from ground truth; using manual selection...\n");
		}		
	}
	if(!init_obj_read && read_obj_from_file) {
		init_objects = cv_utils.readObjectsFromFile(1, read_obj_fname.c_str(), debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		if(img_source == SRC_IMG || img_source == SRC_DISK || img_source == SRC_VID){
			init_objects = cv_utils.getMultipleObjects(input->getFrame(), 1,
				patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname.c_str());
		} else{
			init_objects = cv_utils.getMultipleObjects(input, 1,
				patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname.c_str());
		}
	}

	cv::Mat init_corners;
	int size_x, size_y;
	if(syn_warp_entire_image){
		size_x = input->getFrameWidth();
		size_y = input->getFrameHeight();
		init_corners.create(2, 4, CV_64FC1);
		//! corners of the image itself
		init_corners.at<double>(0, 0) = 0;
		init_corners.at<double>(0, 1) = size_x - 1;
		init_corners.at<double>(0, 2) = size_x - 1;
		init_corners.at<double>(0, 3) = 0;
		init_corners.at<double>(1, 0) = 0;
		init_corners.at<double>(1, 1) = 0;
		init_corners.at<double>(1, 2) = size_y - 1;
		init_corners.at<double>(1, 3) = size_y - 1;
	} else{
		init_corners = init_objects[0]->corners.clone();
		size_x = init_objects[0]->size_x;
		size_y = init_objects[0]->size_y;
	}

	mtf::utils::printMatrix<double>(init_corners, "init_corners");

	cv::Point fps_origin(10, 20);
	double fps_font_size = 1.00;
	cv::Scalar fps_color(0, 255, 0);
	cv::Point err_origin(10, 40);
	double err_font_size = 1.00;
	cv::Scalar err_color(0, 255, 0);
	cv::Scalar gt_color(0, 255, 0);

	resx = size_x;
	resy = size_y;

	mtf::StateSpaceModel *ssm = mtf::getSSM(mtf_ssm);
	if(!ssm){
		printf("State space model could not be initialized");
		return EXIT_FAILURE;
	}
	ssm->initialize(init_corners);
	cv::Mat original_corners(2, 4, CV_64FC1);
	ssm->getCorners(original_corners);
	mtf::PtsT original_pts = ssm->getPts();


	mtf::AppearanceModel *am = mtf::getAM("ssd3", mtf_ilm);
	if(!am){
		printf("Appearance model could not be initialized");
		return EXIT_FAILURE;
	}
	pre_proc_type = "none";
	PreProc* pre_proc = getPreProcObj(am->inputType(), pre_proc_type);
	pre_proc->initialize(input->getFrame());
	am->setCurrImg(pre_proc->getFrame());
	am->initializePixVals(ssm->getPts());
	const mtf::PixValT &original_patch = am->getInitPixVals();

	//mtf::utils::printMatrix(original_patch, "original_patch");
	mtf::utils::printMatrix<double>(original_corners, "original_corners");

	//! generate random warp
	mtf::vectorvd syn_ssm_sigma, syn_ssm_mean;
	VectorXd state_sigma(ssm->getStateSize());
	if(syn_pix_sigma > 0){
		ssm->estimateStateSigma(state_sigma, syn_pix_sigma);
	} else{
		getSamplerParams(syn_ssm_sigma, syn_ssm_mean, syn_ssm_sigma_ids, syn_ssm_mean_ids, "Synthetic");
		state_sigma = Map<const VectorXd>(syn_ssm_sigma[0].data(), ssm->getStateSize());
	}
	VectorXd state_mean = VectorXd::Zero(ssm->getStateSize());
	VectorXd ssm_perturbation(ssm->getStateSize());
	ssm->initializeSampler(state_sigma, state_mean);

	std::string out_seq_name = source_name + "_" + syn_out_suffix;
	std::string out_dir = cv::format("%s/Synthetic/%s", db_root_path.c_str(), out_seq_name.c_str());
	if(!fs::exists(out_dir)){
		printf("Output directory: %s does not exist. Creating it...\n", out_dir.c_str());
		fs::create_directories(out_dir);
	}
	std::string syn_gt_path = cv::format("%s/Synthetic/%s.txt", db_root_path.c_str(), out_seq_name.c_str());
	printf("Writing synthetic sequence GT to: %s\n", syn_gt_path.c_str());

	printf("n_pts: %d\n", ssm->getNPts());
	printf("n_pix: %d\n", original_patch.size());

	//! for OpenCV imwrite function
	vector<int> compression_params;
	compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
	compression_params.push_back(100);

	if(syn_use_inv_warp){
		printf("Using inverse warping method\n");
	}
	cv::Mat warped_bounding_box(2, 4, CV_64FC1), original_bounding_box(2, 4, CV_64FC1);
	int nearest_pt_ids[4];
	if(syn_warp_entire_image){
		for(int corner_id = 0; corner_id < 4; ++corner_id){
			nearest_pt_ids[corner_id] = mtf::utils::getNearestPt(init_objects[0]->corners.at<double>(0, corner_id),
				init_objects[0]->corners.at<double>(1, corner_id), original_pts, ssm->getNPts());

			original_bounding_box.at<double>(0, corner_id) = original_pts(0, nearest_pt_ids[corner_id]);
			original_bounding_box.at<double>(1, corner_id) = original_pts(1, nearest_pt_ids[corner_id]);
		}
	} else{
		original_bounding_box = init_objects[0]->corners;
	}
	//! original image is the first frame in the synthetic sequence
	cv::imwrite(cv::format("%s/frame%05d.jpg", out_dir.c_str(), 1), input->getFrame(), compression_params);
	FILE *syn_gt_fid = fopen(syn_gt_path.c_str(), "w");
	mtf::utils::writeCorners(syn_gt_fid, original_bounding_box, 0, true);
	const char* warped_img_win_name = "warped_img";

	for(int frame_id = 1; frame_id <= syn_n_frames; ++frame_id){

		ssm->generatePerturbation(ssm_perturbation);
		/**
		the SSM will NOT resize any of its output arguments or even check
		if they have the correct size (except in debugging version) so passing
		an argument with iincorrect size will lead to a segmentation fault
		*/
		VectorXd inv_perurbation(ssm->getStateSize());
		ssm->invertState(inv_perurbation, ssm_perturbation);
		//! apply warp perturbation to the SSM
		ssm->compositionalUpdate(ssm_perturbation);
		mtf::PtsT warped_pts = ssm->getPts();
		//! reset the SSM to its original state
		ssm->compositionalUpdate(inv_perurbation);

		//mtf::utils::printMatrixToFile(warped_pts, "warped_pts", "log/syn_log.txt");
		if(syn_warp_entire_image){
			/**
			if the entire image has been warped, we need to find the points in the image-wide grid
			that are nearest to the original bounding box corners and use the warped points corresponding to
			these as the warped bounding box corners
			*/
			for(int corner_id = 0; corner_id < 4; ++corner_id){
				warped_bounding_box.at<double>(0, corner_id) = warped_pts(0, nearest_pt_ids[corner_id]);
				warped_bounding_box.at<double>(1, corner_id) = warped_pts(1, nearest_pt_ids[corner_id]);
			}
		} else{
			/**
			if only the bounding box has been warped, SSM corners are identical to the warped bounding box corners
			*/
			ssm->getCorners(warped_bounding_box);
		}
		cv::Mat warped_img(input->getFrameHeight(), input->getFrameWidth(), CV_8UC3);
		warped_img.setTo(cv::Vec3b(0, 0, 0));
		if(syn_use_inv_warp){
			//! apply inverse of the warp perturbation to the SSM
			ssm->compositionalUpdate(inv_perurbation);
			//am->updatePixVals(ssm->getPts());
			//const mtf::PixValT &warped_patch = am->getCurrPixVals();
			mtf::utils::generateInverseWarpedImg(warped_img, ssm->getPts(),
				am->getCurrImg(), original_pts, am->getImgWidth(), am->getImgHeight(),
				ssm->getNPts(), syn_show_output, warped_img_win_name);
			//! reset the SSM to the original state
			ssm->compositionalUpdate(ssm_perturbation);
		} else{
			cv::Mat warped_corners(2, 4, CV_64FC1);
			ssm->getCorners(warped_corners);
			mtf::utils::generateWarpedImg(warped_img, warped_corners, warped_pts,
				original_patch, input->getFrame(), input->getFrameWidth(),
				input->getFrameWidth(), ssm->getNPts(),
				syn_background_type, syn_show_output, warped_img_win_name);
		}		
		if(syn_show_output){
			cv::Mat original_img = input->getFrame().clone();
			mtf::utils::drawRegion(original_img, original_bounding_box,
				cv::Scalar(0, 255, 0), line_thickness, nullptr, fps_font_size,
				show_corner_ids, 1 - show_corner_ids);
			mtf::utils::drawRegion(original_img, warped_bounding_box,
				cv::Scalar(0, 0, 255), line_thickness, nullptr, fps_font_size,
				show_corner_ids, 1 - show_corner_ids);
			cv::Mat warped_img_annotated = warped_img.clone();
			mtf::utils::drawRegion(warped_img_annotated, warped_bounding_box,
				cv::Scalar(0, 0, 255), line_thickness, nullptr, fps_font_size,
				show_corner_ids, 1 - show_corner_ids);
			cv::imshow("original_img", original_img);
			cv::imshow(warped_img_win_name, warped_img_annotated);
			if(cv::waitKey(500) == 27){ break; }
		}
		cv::imwrite(cv::format("%s/frame%05d.jpg", out_dir.c_str(), frame_id + 1),
			warped_img, compression_params);
		mtf::utils::writeCorners(syn_gt_fid, warped_bounding_box, frame_id);
		printf("Done frame %d\n", frame_id);
	}
	fclose(syn_gt_fid);
	return EXIT_SUCCESS;
}
