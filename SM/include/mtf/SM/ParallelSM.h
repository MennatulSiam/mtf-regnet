#ifndef MTF_PARALLEL_SM_H
#define MTF_PARALLEL_SM_H

#include "SearchMethod.h"
#include "mtf/SM/ParallelParams.h"

_MTF_BEGIN_NAMESPACE

/**
run multiple search methods in parallel with the same AM/SSM; although the code here
is currently identical to ParallelTracker, a seperate module exists for future extensions
that take advantage of the additional information that is available to this class through
direct access to the underlying AM and SSM
*/
template<class AM, class SSM>
class ParallelSM : public SearchMethod<AM, SSM> {

public:
	typedef ParallelParams ParamType;
	typedef ParamType::EstimationMethod EstimationMethod;

	ParamType params;

	typedef SearchMethod<AM, SSM> SM;
	using SM::name;
	using SM::cv_corners_mat;
	using SM::ssm;
	using SM::spi_mask;

	using typename SM::SSMParams;	

	ParallelSM(const vector<SM*> _trackers, const ParamType *parl_params,
		int resx, int resy, const SSMParams *ssm_params);
	void setImage(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override { return cv_corners_mat; }
	void setRegion(const cv::Mat& corners)  override;
	int inputType() const override{ return input_type; }

	void setSPIMask(const bool *_spi_mask)  override;
	void clearSPIMask()  override;
	void setInitStatus()  override;
	void clearInitStatus()  override;
	bool supportsSPI() override;
	// there is no clear answer to what the AM for a parallel chain of SMs should be,
	AM* getAM() const override { return trackers[n_trackers-1]->getAM(); }
	SSM* getSSM() const override { return ssm; }

protected:

	vector<cv::Mat> img_buffer, corners_buffer;
	int buffer_id;
	bool buffer_filled;
	cv::Mat curr_img;

	const vector<SM*> trackers;
	int n_trackers;
	int input_type;

	cv::Mat mean_corners_cv;

	int ssm_state_size;
	std::vector<VectorXd> ssm_states;
	VectorXd mean_state;

	bool failure_detected;
};


_MTF_END_NAMESPACE

#endif

