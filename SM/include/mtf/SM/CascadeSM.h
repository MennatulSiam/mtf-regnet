#ifndef MTF_CASCADE_SM_H
#define MTF_CASCADE_SM_H

#include "SearchMethod.h"
#include "mtf/SM/CascadeParams.h"

_MTF_BEGIN_NAMESPACE

/**
run multiple search methods in cascade with the same AM/SSM; although the code here 
is currently identical to CascadeTracker, a seperate module exists for future extensions 
that take advantage of the additional information that is available to the Cascade through 
direct access to the underlying AM and SSM
*/
template<class AM, class SSM>
class CascadeSM : public SearchMethod < AM, SSM > {

public:
	typedef SearchMethod<AM, SSM> SM;
	typedef CascadeParams ParamType;
	ParamType params;

	using SM::name;
	using SM::spi_mask;

	const vector<SM*> trackers;
	int n_trackers;
	int input_type;

	CascadeSM(const vector<SM*> _trackers, const ParamType *casc_params);
	void initialize(const cv::Mat &corners) override;
	void update() override;
	void setImage(const cv::Mat &img) override;
	void setRegion(const cv::Mat& corners)  override;
	const cv::Mat& getRegion()  override{ return trackers[n_trackers - 1]->getRegion(); }

	void setSPIMask(const bool *_spi_mask)  override;
	void clearSPIMask()  override;
	void setInitStatus()  override;
	void clearInitStatus()  override;
	bool supportsSPI() override;
	int inputType() const override{ return input_type; }
	// though there is no clear answer to what the AM and SSM for a cascade of SMs should be,
	// those of the last tracker in the cascade may make some sense expecially for the SSM
	AM* getAM() const override { return trackers[n_trackers - 1]->getAM(); }
	SSM* getSSM() const override { return trackers[n_trackers - 1]->getSSM(); }

protected:
	bool failure_detected;
	vector<cv::Mat> img_buffer, corners_buffer;
	int buffer_id;
	bool buffer_filled;
	cv::Mat curr_img;

	void updateTrackers(const cv::Mat &img);
};
_MTF_END_NAMESPACE

#endif

