#ifndef MTF_RKLT_H
#define MTF_RKLT_H

#include "SearchMethod.h"
#include "GridBase.h"

#define RKLT_ENABLE_SPI true
#define RKLT_ENABLE_FEEDBACK true
#define RKLT_FAILURE_DETECTION true
#define RKLT_FAILURE_THRESH 15.0
#define RKLT_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct RKLTParams{
	bool enable_spi; 
	bool enable_feedback;
	bool failure_detection;
	double failure_thresh;
	bool debug_mode;

	RKLTParams(bool _enable_spi, bool _enable_feedback,
		bool _failure_detection, double _failure_thresh,
		bool _debug_mode);
	RKLTParams(const RKLTParams *params = nullptr);
};

template<class AM, class SSM>
class RKLT : public CompositeBase {

public:

	typedef SearchMethod < AM, SSM > TemplTrackerType;

	typedef RKLTParams ParamType;
	ParamType params;

	TemplTrackerType *templ_tracker;
	GridBase * grid_tracker;
	cv::Mat grid_corners_mat;

	RKLT(const ParamType *rklt_params,
		GridBase *_grid_tracker, TemplTrackerType *_templ_tracker);

	void initialize(const cv::Mat &corners) override;
	void update() override;
	void setImage(const cv::Mat &cv_img) override;
	void setRegion(const cv::Mat &corners) override;

private:
	~RKLT(){}
};
_MTF_END_NAMESPACE

#endif

