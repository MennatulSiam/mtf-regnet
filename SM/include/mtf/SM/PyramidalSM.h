#ifndef MTF_PYRAMIDAL_SM_H
#define MTF_PYRAMIDAL_SM_H

#include "SearchMethod.h"
#include "mtf/SM/PyramidalParams.h"

_MTF_BEGIN_NAMESPACE

/**
run multiple search methods with the same AM/SSM on a Gaussian image pyramid; although the code here
is currently identical to CascadeTracker, a seperate module exists for future extensions
that take advantage of the additional information that is available to the Cascade through
direct access to the underlying AM and SSM
*/
template<class AM, class SSM>
class PyramidalSM : public SearchMethod < AM, SSM > {

public:
	typedef SearchMethod<AM, SSM> SM;
	typedef PyramidalParams ParamType;
	ParamType params;

	const vector<SM*> trackers;
	int n_trackers;

	using SM::name;
	using SM::spi_mask;
	using SM::cv_corners_mat;

	vector<cv::Size> img_sizes;
	vector<cv::Mat> img_pyramid;
	double overall_scale_factor;
	bool external_img_pyramid;

	PyramidalSM(const vector<SM*> &_trackers, 
		const ParamType *parl_params);
	void setImage(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override { 
		return trackers[0]->getRegion();
	}
	void setRegion(const cv::Mat& corners)  override;
	int inputType() const override{
		return trackers[0]->inputType();
	}

	void setSPIMask(const bool *_spi_mask)  override;
	void clearSPIMask()  override;
	void setInitStatus()  override;
	void clearInitStatus()  override;
	bool supportsSPI() override;
	/**
	though there is no clear answer to what the AM and SSM for a cascade of SMs should be,
	those of the first tracker in the pyramid may make some sense expecially for the SSM
	*/
	AM* getAM() const override { return trackers[0]->getAM(); }
	SSM* getSSM() const override { return trackers[0]->getSSM(); }

protected:
	void setImagePyramid(const vector<cv::Mat> &_img_pyramid);
	const vector<cv::Mat>& getImagePyramid() const{ return img_pyramid; }
	void updateImagePyramid();
	void showImagePyramid();
};

_MTF_END_NAMESPACE

#endif

