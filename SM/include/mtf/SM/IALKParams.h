#ifndef MTF_IALK_PARAMS_H
#define MTF_IALK_PARAMS_H

#include "mtf/Macros/common.h"

#define IALK_MAX_ITERS 10
#define IALK_EPSILON 0.01
#define IALK_HESS_TYPE 0
#define IALK_SEC_ORD_HESS false
#define IALK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct IALKParams{
	enum class HessType{ InitialSelf, CurrentSelf, Std };
	int max_iters; //! maximum iterations of the IALK algorithm to run for each frame
	double epsilon; //! maximum L1 norm of the state update vector at which to stop the iterations
	HessType hess_type;
	bool sec_ord_hess;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	IALKParams(int _max_iters, double _epsilon,
		HessType _hess_type, bool _sec_ord_hess,
		bool _debug_mode);
	IALKParams(const IALKParams *params = nullptr);
};

_MTF_END_NAMESPACE

#endif

