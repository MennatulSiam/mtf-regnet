* RANSAC for affine, similitude, transcaling and isometry
* Windows support - adapt GNU make system for Visual Studio
* Matlab front end
* HTML/Javascript front end for web based demo
* Augmented reality application
* Multi channel support for LSCV and LRSCV
* Channel wise implementations of multi channel support for all AMs especially MI and CCRE
* Complete KL Divergence AM and it localized version (LKLD)
* Improve Doxygen documentation
* Selective pixel integration support for all AMs and SSMs
* RBF illumination model
* Complete Spline and TPS SSM
* Add piecewise projective and affine SSMs
