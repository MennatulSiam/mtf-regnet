var searchData=
[
  ['pcaparams',['PCAParams',['../structPCAParams.html#a4daa094b84ee0f8e8311e9598f806386',1,'PCAParams::PCAParams(const ImgParams *img_params, string _basis_data_root_dir, string _actor, string _seq_name, int _extraction_id, int _n_feat, int _len_history)'],['../structPCAParams.html#a8a0885e3a10e37357d45fe3a49f78d63',1,'PCAParams::PCAParams(const PCAParams *params=nullptr)']]],
  ['preprocess_5fbatch',['preprocess_batch',['../classutils_1_1MTFNet.html#a8b235e2ad602dce19a2da88b43072f2a',1,'utils::MTFNet']]],
  ['processdistributions',['processDistributions',['../structmtf_1_1NNParams.html#acfd3184821a45e10e5907b691b1c793c',1,'mtf::NNParams::processDistributions()'],['../structPFParams.html#a00a5be7ba67294d100efffa9a6613aed',1,'PFParams::processDistributions()']]]
];
