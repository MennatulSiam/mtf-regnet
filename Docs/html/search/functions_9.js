var searchData=
[
  ['miparams',['MIParams',['../structMIParams.html#a02118b495e11b2b086dfbbea4f06a3e4',1,'MIParams::MIParams(const ImgParams *img_params, int _n_bins, double _pre_seed, bool _partition_of_unity, ImageBase *_pix_mapper, bool _debug_mode)'],['../structMIParams.html#a7d61e0e505a06adcc6631c70ea115735',1,'MIParams::MIParams(const MIParams *params=nullptr)']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html#ab247e07b81153a7c0da1a4c225894828',1,'utils::MTFNet']]]
];
