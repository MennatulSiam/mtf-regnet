var searchData=
[
  ['scvparams',['SCVParams',['../structSCVParams.html#ace2f7974d4f8ba45b93113aa8cc12f53',1,'SCVParams::SCVParams(const ImgParams *img_params, bool _use_bspl, int _n_bins, double _pre_seed, bool _partition_of_unity, bool _weighted_mapping, bool _mapped_gradient, bool _approx_dist_feat, bool _debug_mode)'],['../structSCVParams.html#ab9ec64f0f919564179aba437c34224df',1,'SCVParams::SCVParams(const SCVParams *params=nullptr)']]],
  ['setcurrimg',['setCurrImg',['../classImageBase.html#a9eb8fba660685c2a74b2cf2658b103ed',1,'ImageBase']]],
  ['setfirstiter',['setFirstIter',['../classAppearanceModel.html#a0cd5810a7ceca3418eaed254b4f11196',1,'AppearanceModel::setFirstIter()'],['../classPCA.html#a0172ae4b030c3327cecbd6c1a6a17d84',1,'PCA::setFirstIter()']]],
  ['setsimilarity',['setSimilarity',['../classAppearanceModel.html#a615e1e41bfe54f3c36d9ea88d2a3da57',1,'AppearanceModel']]],
  ['sl3params',['SL3Params',['../structSL3Params.html#a8d5ecf8fcd45553ffb94468e1953efdf',1,'SL3Params::SL3Params(bool _normalized_init, bool _iterative_sample_mean, int _sample_mean_max_iters, double _sample_mean_eps, bool _debug_mode)'],['../structSL3Params.html#acff7e46460a9b098b6e22362cf56ab1a',1,'SL3Params::SL3Params(const SL3Params *params=nullptr)']]],
  ['supportsspi',['supportsSPI',['../classAppearanceModel.html#a119b97402d3bb7041d921961e3722cf4',1,'AppearanceModel::supportsSPI()'],['../classSSDBase.html#ac020668a264146f49d8aaed6235da77e',1,'SSDBase::supportsSPI()']]]
];
