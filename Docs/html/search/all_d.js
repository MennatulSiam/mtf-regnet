var searchData=
[
  ['max_5fiters',['max_iters',['../structmtf_1_1NNParams.html#aa975a023178ef9da04c0ef89eedf745b',1,'mtf::NNParams::max_iters()'],['../structPFParams.html#a247f6354e907eac4019b5f194dc00e53',1,'PFParams::max_iters()']]],
  ['max_5fsimilarity',['max_similarity',['../classPF.html#a8d03c6cfb90bf9f711d5a80aac777835',1,'PF']]],
  ['max_5fwt_5fid',['max_wt_id',['../classPF.html#a6ccad6776f706d957ab7b2f878d18d33',1,'PF']]],
  ['mcccre',['MCCCRE',['../classMCCCRE.html',1,'']]],
  ['mcmi',['MCMI',['../classMCMI.html',1,'']]],
  ['mcncc',['MCNCC',['../classMCNCC.html',1,'']]],
  ['mcrscv',['MCRSCV',['../classMCRSCV.html',1,'']]],
  ['mcscv',['MCSCV',['../classMCSCV.html',1,'']]],
  ['mcspss',['MCSPSS',['../classMCSPSS.html',1,'']]],
  ['mcssd',['MCSSD',['../classMCSSD.html',1,'']]],
  ['mcssim',['MCSSIM',['../classMCSSIM.html',1,'']]],
  ['mczncc',['MCZNCC',['../classMCZNCC.html',1,'']]],
  ['mean_5ftype',['mean_type',['../structPFParams.html#adef741dc9861c4a22ee8543d0b383221',1,'PFParams']]],
  ['mi',['MI',['../classMI.html',1,'']]],
  ['min_5fparticles_5fratio',['min_particles_ratio',['../structPFParams.html#a16a7f4688337858b1ef814f08956afba',1,'PFParams']]],
  ['miparams',['MIParams',['../structMIParams.html',1,'MIParams'],['../structMIParams.html#a02118b495e11b2b086dfbbea4f06a3e4',1,'MIParams::MIParams(const ImgParams *img_params, int _n_bins, double _pre_seed, bool _partition_of_unity, ImageBase *_pix_mapper, bool _debug_mode)'],['../structMIParams.html#a7d61e0e505a06adcc6631c70ea115735',1,'MIParams::MIParams(const MIParams *params=nullptr)']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html#ab247e07b81153a7c0da1a4c225894828',1,'utils::MTFNet']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html',1,'utils']]]
];
