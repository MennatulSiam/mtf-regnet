var searchData=
[
  ['nccparams',['NCCParams',['../structNCCParams.html#aa9afca232c36019f81401e35b3c92fff',1,'NCCParams::NCCParams(const ImgParams *img_params, bool _fast_hess)'],['../structNCCParams.html#a42ac2c56df37193dbdd567eab5ad1f06',1,'NCCParams::NCCParams(const NCCParams *params=nullptr)']]],
  ['nssdparams',['NSSDParams',['../structNSSDParams.html#a23eb73104bc646114bd923909da4dfe1',1,'NSSDParams::NSSDParams(const ImgParams *img_params, double _norm_pix_max, double _norm_pix_min, bool _debug_mode)'],['../structNSSDParams.html#a5ac90a50027d4d8ba7fb1c1757e46444',1,'NSSDParams::NSSDParams(const NSSDParams *params=nullptr)']]]
];
