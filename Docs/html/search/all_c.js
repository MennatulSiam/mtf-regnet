var searchData=
[
  ['learning_5frate',['learning_rate',['../structFCGDParams.html#a06e6a67f6288c40b1d8ad4d92a1f2e2a',1,'FCGDParams']]],
  ['len_5fhistory',['len_history',['../structPCAParams.html#a17b133133e534514d088b902d2ed31f4',1,'PCAParams']]],
  ['lieaffine',['LieAffine',['../classLieAffine.html',1,'']]],
  ['lieaffineparams',['LieAffineParams',['../structLieAffineParams.html',1,'LieAffineParams'],['../structLieAffineParams.html#a277011f38bc1cfe72b5b0c300c7e0932',1,'LieAffineParams::LieAffineParams(bool _init_as_basis, bool _debug_mode)'],['../structLieAffineParams.html#aefc94b1c35befae2da3e15d87399b544',1,'LieAffineParams::LieAffineParams(LieAffineParams *params=nullptr)']]],
  ['liehomography',['LieHomography',['../classLieHomography.html',1,'']]],
  ['liehomographyparams',['LieHomographyParams',['../structLieHomographyParams.html',1,'LieHomographyParams'],['../structLieHomographyParams.html#a591827d84931a9d699481ece04ccc9c3',1,'LieHomographyParams::LieHomographyParams(bool _init_as_basis, bool _debug_mode)'],['../structLieHomographyParams.html#abeb5b3804b293aa46dbb1535b8809142',1,'LieHomographyParams::LieHomographyParams(const LieHomographyParams *params=nullptr)']]],
  ['lieisometry',['LieIsometry',['../classLieIsometry.html',1,'']]],
  ['lieisometryparams',['LieIsometryParams',['../structLieIsometryParams.html',1,'LieIsometryParams'],['../structLieIsometryParams.html#a07f5b6928267def52468148f4eb981c3',1,'LieIsometryParams::LieIsometryParams(bool _debug_mode, bool _init_as_basis)'],['../structLieIsometryParams.html#a283eb430f11384458003449dea75151f',1,'LieIsometryParams::LieIsometryParams(LieIsometryParams *params=nullptr)']]],
  ['likelihood_5falpha',['likelihood_alpha',['../structZNCCParams.html#aecf018afbe10fb4062ea0a0993bfb127',1,'ZNCCParams']]],
  ['lineparams',['lineParams',['../structlineParams.html',1,'']]],
  ['linetracker',['LineTracker',['../classLineTracker.html',1,'']]],
  ['linetrackerparams',['LineTrackerParams',['../structLineTrackerParams.html',1,'']]],
  ['lkld',['LKLD',['../classLKLD.html',1,'']]],
  ['lkldparams',['LKLDParams',['../structLKLDParams.html',1,'LKLDParams'],['../structLKLDParams.html#a57b2871d9bbc2a880f72ea71650b4692',1,'LKLDParams::LKLDParams(const ImgParams *img_params, int _n_sub_regions_x, int _n_sub_regions_y, int _spacing_x, int _spacing_y, int _n_bins, double _pre_seed, bool _partition_of_unity, bool _debug_mode)'],['../structLKLDParams.html#afec38fd147a84cde5192218cc39932fa',1,'LKLDParams::LKLDParams(const LKLDParams *params=nullptr)']]],
  ['lrscv',['LRSCV',['../classLRSCV.html',1,'']]],
  ['lrscvparams',['LRSCVParams',['../structLRSCVParams.html',1,'LRSCVParams'],['../structLRSCVParams.html#ae5b5e0e6826996cf6d8b9cefe519e217',1,'LRSCVParams::LRSCVParams(const ImgParams *img_params, int _sub_regions_x, int _sub_regions_y, int _spacing_x, int _spacing_y, bool _affine_mapping, bool _once_per_frame, int _n_bins, double _pre_seed, bool _weighted_mapping, bool _show_subregions, bool _debug_mode)'],['../structLRSCVParams.html#a9c47e58f1ace93504f0359618d6edcca',1,'LRSCVParams::LRSCVParams(const LRSCVParams *params=nullptr)']]],
  ['lscv',['LSCV',['../classLSCV.html',1,'']]],
  ['lscvparams',['LSCVParams',['../structLSCVParams.html',1,'LSCVParams'],['../structLSCVParams.html#ad353810c0115756103ee92369338b9c8',1,'LSCVParams::LSCVParams(const ImgParams *img_params, int _sub_regions_x, int _sub_regions_y, int _spacing_x, int _spacing_y, bool _affine_mapping, bool _once_per_frame, int _n_bins, double _pre_seed, bool _weighted_mapping, bool _show_subregions, bool _debug_mode)'],['../structLSCVParams.html#a6c5efcf758424874d905a0367488b643',1,'LSCVParams::LSCVParams(const LSCVParams *params=nullptr)']]]
];
