var searchData=
[
  ['rscvparams',['RSCVParams',['../structRSCVParams.html#a3e71e8e1ccfd7579c3f74b35bda9e32a',1,'RSCVParams::RSCVParams(const ImgParams *img_params, bool _use_bspl, int _n_bins, double _pre_seed, bool _partition_of_unity, bool _weighted_mapping, bool _mapped_gradient, bool _approx_dist_feat, bool _debug_mode)'],['../structRSCVParams.html#addd6cd24344dfc1383ab659943bf0691',1,'RSCVParams::RSCVParams(const RSCVParams *params=nullptr)']]]
];
