#include "mtf/AM/SSD.h"

_MTF_BEGIN_NAMESPACE

SSD::SSD(const ParamType *ssd_params, const int _n_channels) :
SSDBase(ssd_params, _n_channels){
	printf("\n");
	printf("Using Sum of Squared Differences AM with...\n");
	printf("grad_eps: %e\n", grad_eps);
	printf("hess_eps: %e\n", hess_eps);
	name = "ssd";
}

_MTF_END_NAMESPACE

