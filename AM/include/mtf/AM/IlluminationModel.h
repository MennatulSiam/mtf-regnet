#ifndef MTF_ILLUMINATION_MODEL_H
#define MTF_ILLUMINATION_MODEL_H

#include "mtf/Macros/common.h"
#include <stdexcept>

_MTF_BEGIN_NAMESPACE
/**
Illumination Model is a parametric function that transforms pixel values extracted 
from a patch to account for illumination changes
g(I, a): R(N) x R(K) -> R(N)
*/
class IlluminationModel{
protected:
	int n_pix;
public:
	string name;
	bool apply_on_init;
	IlluminationModel(){}
	virtual ~IlluminationModel(){}
	virtual int getStateSize() const = 0;
	virtual void initialize(double *p, int _n_pix){ n_pix = _n_pix; }
	virtual void apply(double *g, const double *I, const double *p) = 0;
	virtual void invert(double *inv_p, double *p) = 0;
	virtual void update(double *new_p, const double *old_p, const double *dp) = 0;
	virtual	void cmptParamJacobian(double *df_dp, const double *df_dg,
		const double *I, const double *p) = 0;
	virtual void cmptPixJacobian(double *df_dI, const double *df_dg,
		const double *I, const double *p) = 0;
	// NULL for d2f_dg2 implies that the corresponding matrix is identity;
	// conversely a non negative return imples an identity d2f_dI2 scaled by the return value;
	// these conventions are necessary to avoid unnecessary and very expensive matrix multiplications;

	// d2f_dp2 = (dg_dp^T*d2f_dg2 + df_dg*d2g_dp2)*dg_dp
	virtual	double cmptParamHessian(double *d2f_dp2, const double *d2f_dg2,
		const double *df_dg, const double *I, const double *p) = 0;
	virtual double cmptPixHessian(double *d2f_dI2, const double *d2f_dg2,
		const double *df_dg, const double *I, const double *p) = 0;
	virtual void cmptCrossHessian(double *d2f_dp_dI, const double *d2f_dg2,
		const double *df_dg, const double *I, const double *p) = 0;
};

_MTF_END_NAMESPACE

#endif



