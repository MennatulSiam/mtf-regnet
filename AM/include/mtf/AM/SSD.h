#ifndef MTF_SSD_H
#define MTF_SSD_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

class SSD : public SSDBase{
public:

	typedef ImgParams ParamType;

	SSD(const ParamType *ssd_params, const int _n_channels = 1);
};

_MTF_END_NAMESPACE

#endif